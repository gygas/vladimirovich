1.
___
Для того что бы запустить проект.
Нам нужно с помощью команд Git загрузить в MuleSoft наш проект. 
___
2.
___
В роли web-service запустить Postman и Mockoon 
В Postman настраиваем на Listener который мы используем
А в  Mockoon указываем GET и sap/opu/odata/sap/ZCDS_LAND_CTRL_CDS/zcds_land_ctrl и сам файл который мы будем передавать.
___
3.
___
Далее настраиваем ELK стек на Mule
Для разворачивания стека нам потребуется:

k/ubuntu версии 20.04 и выше

docker версии не ниже 20.10.12

docker-compose версии не ниже 1.29.2

Для запуска ELK создадим директорию и в console вводим данные команды:

sudo mkdir /srv/elk
cd /srv/elk

После создайте файл /srv/elk/docker-compose.yaml
sudo -e /srv/elk/docker-compose.yaml
со следующим содержимым:


```

version: '3'
services:

  elasticsearch:
    image: elastic/elasticsearch:8.2.0
    restart: always
    volumes:
     - "./elasticsearch/data:/usr/share/elasticsearch/data"
     - "./elasticsearch/logs:/usr/share/elasticsearch/logs"
    ports:
      - "0.0.0.0:9200:9200"
      - "0.0.0.0:9300:9300"
    ulimits:
      memlock:
        soft: -1
        hard: -1
    environment:
      - cluster.name=docker-cluster
      - node.name=es1
      - cluster.initial_master_nodes=es1
      - bootstrap.memory_lock=true
      - "ES_JAVA_OPTS=-Xms1g -Xmx1g -Xlog:disable -Xlog:all=warning:stderr:utctime,level,tags -Xlog:gc=warning:stderr:utctime"
      - xpack.ml.enabled=false
      - xpack.security.enabled=false
      - ELASTIC_PASSWORD=changeme
    restart: always

  kibana:
    image: kibana:8.2.0
    restart: always
    volumes:
      - "./kibana/data:/usr/share/kibana/data"
    environment:
      - KIBANA_SYSTEM_PASSWORD=changeme
    ports:
      - "0.0.0.0:5601:5601"


  logstash:
    image: elastic/logstash:8.2.0
    volumes:
      - "./logstash/config:/usr/share/logstash/config:ro"
    ports:
      - "5046:5046/tcp"
      - "5046:5046/udp"
    environment:
      - LOGSTASH_INTERNAL_PASSWORD=changeme
    restart: always



```

Чтобы разместить файлы конфигураций и данных необходимо заранее создать директории и назначить им права доступ

```
sudo mkdir -p ./elasticsearch/data \
         ./elasticsearch/logs \
         ./kibana/data \
         ./logstash/config

sudo chmod 777 ./elasticsearch/data \
          ./elasticsearch/logs \
          ./kibana/data \
          ./logstash/config
          


```
Создайте файл logstash/config/logstash.yml:

touch /srv/elk/logstash/config/logstash.yml
echo 'path.config: "./config/config.conf"' > /srv/elk/logstash/config/logstash.yml

Теперь создайте файл logstash/config/config.conf

```
input {
  tcp {
    port => "5046"
    codec => json
  }
  udp {
    port => "5046"
    codec => json
  }
}
filter {
    json {
      source => "message"
      skip_on_invalid_json => false
    }
}
output {
    elasticsearch {
      hosts => ["elasticsearch:9200"]
      ilm_rollover_alias => "logs-%{+yyyy.MM.dd}"
      ilm_pattern => "000001"
  }
}
```

Теперь запустим проект следующей командой:
docker-compose up -d
Дальше мы настроим наши компоненты логер в Mule что бы они по умолчанию отправляли логи в ELK
По-умолчанию при создании пустого проекта в папке src/main/resources лежит файл log4j2.xml:
Настроим log4j2.xml для отправки логов по протоколу UDP:

```
<?xml version="1.0" encoding="utf-8"?>
<Configuration>
    <Appenders>
        <Socket name="logstash" host="localhost" port="5046" protocol="udp">
            <JsonLayout properties="true" />
        </Socket>
    </Appenders>

    <Loggers>
        <AsyncLogger name="org.mycompany.mylogs" level="INFO" additivity="false">
            <AppenderRef ref="logstash" />
        </AsyncLogger>

        <AsyncRoot level="WARN">
            <AppenderRef ref="logstash" />
        </AsyncRoot>
    </Loggers>
</Configuration>

```


Настроим конфигурацию logstash:
```

input {
	udp {
		port => 5046
		codec => "json"
	}
}
filter {
	mutate {
		remove_field => [ "event", "instant" ]
	}
	json {
		source => "message"
		remove_field => "message"
		target => "m2j"
	}
}
output {
	elasticsearch {
		hosts => "elasticsearch:9200"
		user => "logstash_internal"
		password => "${LOGSTASH_INTERNAL_PASSWORD}"
	}
}

```
___
4.
___
Далее нам нужен Prometheus и Grafana для того, что бы отслеживать наши метрики и графики.
из папки где установлен Prometheus запускаем наши контейнеры с помощью команды:
docker-compose up -d
___
5.
___
Следущий этап, мы подключаем брокер RabbitMQ, c помощью команды
docker run -d -p 15672:15672 -p 5672:5672 --hostname my-rabbit --name some-rabbit rabbitmq:3-management
___
6.
___

Заключительный этап
Нам нужно подключиться к pgAdmin - PostgreSQL, что бы файлы туда переносились.

И теперь когда мы будем стучать в наш job через Postman, нам будет приходить наш файл который мы передаем, часть из них будет проходить через Update часть через insert, это мы уже увидим как и по метрикам в Prometheus так и можем их визуализировать в Grafana.
